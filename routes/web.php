<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});



Route::middleware(['auth','DateMiddleWare'])->group(function(){
    Route::get('/route-1','TestController@test');
});

Route::middleware(['auth','admin'])->group(function(){
    Route::get('/route-2','TestController@admin');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/

//use Illuminate\Routing\Route;

// Route::get('/',function(){
//     return view('app');
// });


Route::view('/{any?}', 'app')->where('any','.*');


