<?php

//use Illuminate\Http\Request;
//use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//use Illuminate\Routing\Route;

Route::namespace('Auth')->group(function(){
    Route::post('register','RegisterController');
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
    Route::post('verification','VerificationController');
    Route::get('regenerate-otp','RegenerateOtpCodeController');
    Route::post('update-password','UpdatePasswordController');
});


Route::namespace('Auth')->middleware('api','email_verified','auth:api')->group(function(){

        Route::get('profile/show','ProfileController@show');
        Route::post('profile/update','ProfileController@update');
});


Route::namespace('Article')->middleware('auth:api')->group(function(){
    Route::post('create-new-article','ArticleController@store');
    Route::patch('update-the-selected-article/{article}','ArticleController@update');
    Route::delete('delete-the-selected-article/{article}','ArticleController@destroy');
});

//Route::namespace('Article')->group(function(){
    Route::get('articles/{article}','Article\ArticleController@show');
    Route::get('articles','Article\ArticleController@index');
//});

Route::get('user','UserController');
