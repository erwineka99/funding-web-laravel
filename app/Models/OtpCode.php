<?php

namespace App\Models;
use App\Traits\UsesUUID;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    use UsesUUID;
    protected $fillable = [
        'user_id', 'otp', 'valid_until'
    ];
}
