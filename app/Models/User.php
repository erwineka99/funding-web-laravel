<?php

namespace App\Models;

use App\Models\Article\Article;
use App\Traits\UsesUUID;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Auth;
use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,UsesUUID;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public static function boot(){
        parent::boot();

        // static::creating(function ($model){
        //     $model->role_id= $model->get_user_role_id();
        // });

        static::created(function ($model){
            $model->generate_otp_code();
        });
    }
    protected $fillable = [
        'name', 'email', 'password','role_id','username','photo_profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin(){
        if($this->role_id==1){
            return true;
        }
        return false;
    }

    public function isVerifiedEmail(){
        if($this->email_verified_at!=null){
            return true;
        }
        return false;
    }

    public function generate_otp_code(){

        do{
            $random= mt_rand(100000,999999);
            $check = OtpCode::where('otp')->first();

        }while($check);

        $now= Carbon::now();

        $otp_code= OtpCode::updateOrCreate(
            ['user_id' => $this->id],
            ['otp' => $random,'valid_until'=> $now->addMinutes(5)],
        );
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function articles(){

        return $this->hasMany(Article::class);
    }



}
