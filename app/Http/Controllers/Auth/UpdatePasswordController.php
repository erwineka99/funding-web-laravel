<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request,[
            'email' => 'email|required',
            'password' => 'required|confirmed|min:6'
        ]);

        $check= User::where('email',$request->email)->first();

        if($check===null){
            $respone=[
                'response_code' => '01',
                'response_message' => 'User tidak ditemukan'
            ];

            return response()->json($respone,200);
        }

        User::where('email',$request->email)
            ->update(['password'=> bcrypt(request('password'))]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'password berhasil diubah'
        ],200);
    }
}
