<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {


        request()->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

         if(!$token = auth()->attempt($request->only('email','password'))){
             return response(['error' => 'Unauthorized'],401);
         }

        // return response()->json(compact('token'));

        $data_request =$request->all();
        //dd($data_request);
        $user = User::where('email', '=', $data_request['email'])->firstOrFail();

        $data['name']=$user->name;

        return response()->json([
            'response_code' => '00',
            'reponse_message' => 'user  berhasil login, silahkan cek email untuk melihat kode otp',
            'data' => $data,
            'token' => $token
        ]);
    }
}
