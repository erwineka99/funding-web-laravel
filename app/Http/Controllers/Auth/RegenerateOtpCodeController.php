<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegenerateOtpEvent;
use App\Http\Controllers\Controller;
use App\Models\OtpCode;
use Illuminate\Http\Request;
use App\Models\User;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate(
            ['email'=>'required',]
        );

        $user= User::where('email',$request->email)->first();

        $user->generate_otp_code();

        $otp= OtpCode::where('user_id',$user->id)->first();
        //dd($otp);
        $data['user']=$user;
        $data['otp']=$otp;
        $otp['email']=$user->email;
        //dd($user);
        event(new RegenerateOtpEvent($otp));

        return response()->json([
            'response_code'=>'00',
            'response_message'=> 'otp berhasil digenerate, silahkan cek email untuk melihat kode otp',
            'data' => $data
        ]);
    }
}
