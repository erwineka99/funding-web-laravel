<?php

namespace App\Listeners;

use App\Events\RegenerateOtpEvent;
use App\Mail\OtpRegenerateMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;


class SendEmailNotificationRegenerateOtp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpEvent $event)
    {

        Mail::to($event->otp)->send(new OtpRegenerateMail($event->otp));
    }
}
