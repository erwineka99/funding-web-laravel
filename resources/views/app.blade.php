<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
</head>
<body>
    <h1>sanbercode </h1>
    <main>

        <div id="app">
            <app></app>
        </div>

    </main>
    <script src={{url('/js/app.js')}}></script>
</body>
</html>
