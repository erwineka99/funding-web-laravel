import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

//DEFINE ROUTE
const router =new Router({
    mode : 'history',
    routes:[
        {
            path: '/',
            name: 'home',
            alias: '/home',
            components:()=> import('./views/Home.vue')
        },
        {
            path: '/donations',
            name: 'donations',
            components:()=> import('./views/Donations.vue')
        },
        {
            path: '*',
            redirect:'/'
        }

    ]
});

export default router
